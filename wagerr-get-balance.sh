#!/bin/bash

DIR="$(dirname "$0")"
. "$DIR/config.cfg"

COIN="WGR"
API_KEY="d86ad5dee7719937"
WAGERR_EXPLORER1="https://explorer.wagerr.com"
WAGERR_EXPLORER2="https://explorer2.wagerr.com"
WAGERR_EXPLORER_NEW="https://chainz.cryptoid.info"
WAGERR_API="https://coinlib.io/api/v1/coin?key=${API_KEY}&pref=EUR&symbol=${COIN}"
WAGERR_API2="https://explorer.wagerr.com/api"
WAGERR_PRICE_EUR=$(curl --silent ${WAGERR_API} | jq '.price' | bc)
#WAGERR_PRICE_EUR=$(curl --silent https://api.coinmarketcap.com/v1/ticker/wagerr/?convert=EUR | jq '.[0].price_eur' | bc)
#WAGERR_MN_LIST=$(curl --silent ${WAGERR_EXPLORER2}/ext/masternodes?_=1520071898184 | jq '.data')
WAGERR_BALANCE_OLD=$(cat /tmp/wagerr_balance.txt 2> /dev/null || echo "${#MN[@]} * 25000" | bc)

TOTAL_M=0
WAGGER_MN_ERROR=0

# Choose Explore
WAGERR_EXPLORER=$WAGERR_EXPLORER_NEW
CODE=$(curl -s -o /dev/null -w "%{http_code}" "$WAGERR_EXPLORER")

if [ "$CODE" != "200" ]; then
  WAGERR_EXPLORER=$WAGERR_EXPLORER2
fi

# Get Balance
for i in "${MN[@]}"
do
  BALANCE=$(curl --silent "$WAGERR_EXPLORER/wgr/api.dws?q=getbalance&a=$i")
  TOTAL_M=$(echo "${TOTAL_M} + ${BALANCE}" | bc)
done

TOTAL=$TOTAL_M

for i in "${A[@]}"
do
  BALANCE=$(curl --silent "$WAGERR_EXPLORER/wgr/api.dws?q=getbalance&a=$i")
  TOTAL=$(echo "${TOTAL} + ${BALANCE}" | bc)
done

TOTAL_F=$(echo "$TOTAL" | bc | xargs printf "%'.0f\n")
TOTAL_W=$(echo "${WAGERR_PRICE_EUR} * ${TOTAL}" | bc | xargs printf "%'.0f\n")

for i in "${MN[@]}"
do
  MASTERNODE_INFO=$(curl -s "${WAGERR_API2}/masternode/${i}")
  MASTERNODE_STATUS=$(echo "$MASTERNODE_INFO" | jq -r '.mns.status')

  if [ "$MASTERNODE_STATUS" != "ENABLED" ]; then
    /usr/local/bin/slackr -c warning "[$COIN]💸 ERROR MN $i - $MASTERNODE_STATUS"
    WAGGER_MN_ERROR=$((WAGGER_MN_ERROR + 1))
  fi
done

if [ "$WAGGER_MN_ERROR" -eq "0" ]; then
  FOOTER="${#MN[@]}/${#MN[@]} MN are healthy"
else
  FOOTER="${WAGGER_MN_ERROR}/${#MN[@]} MN have an error"
fi

TITLE="[$COIN] Total $TOTAL_F - 🏦 $TOTAL_W €"

# Earn
MINED=$(echo "${TOTAL_M} - ${WAGERR_BALANCE_OLD}" | bc | xargs printf "%'.2f\n" )
SOME_EARN=$(echo $MINED'>'0 | bc -l)

if [ "$SOME_EARN" -gt "0" ]; then
  EARNED=$(echo "${WAGERR_PRICE_EUR} * ${MINED}" | bc | xargs printf "%0.2f\n")
  /usr/local/bin/slackr -f "$FOOTER" -t "$TITLE" -c good "💰$MINED Earned - $EARNED €"
else
  /usr/local/bin/slackr -f "$FOOTER" -t "$TITLE" -c warning "💸 Nothing today"
fi

echo $TOTAL_M > /tmp/wagerr_balance.txt